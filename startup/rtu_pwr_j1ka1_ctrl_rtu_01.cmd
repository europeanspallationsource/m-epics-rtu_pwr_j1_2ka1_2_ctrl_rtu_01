# @field RTUNAME
# @type STRING
# RTU name, should be the same as the name in CCDB

# This snippets loads a few records that are specific for J1 substation (all other records are identical in J1 and J2)
# Load the database defining your EPICS records
dbLoadRecords(rtu_pwr_j1ka1_ctrl_rtu_01.db, "RTUNAME = $(RTUNAME)")
