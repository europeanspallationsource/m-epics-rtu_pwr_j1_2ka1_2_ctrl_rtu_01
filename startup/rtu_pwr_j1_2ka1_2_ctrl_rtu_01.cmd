# @field RTUNAME
# @type STRING
# RTU name, should be the same as the name in CCDB

# @field SUBSTATIONNAME
# @type STRING
# Substation name

# @field IPADDR
# @type STRING
# RTU IP address

# @field MODBUSDRVPORT
# @type INTEGER
# RTU comms port for EPICS modbus driver connection, should be the same as configured in RTU connection configuration (default = 502)

# @field RECVTIMEOUT
# @type INTEGER
# RTU->EPICS receive timeout (ms), should be longer than frequency of RTU SND block trigger (REQ input)


########## Set up modbus driver ##########
#Specify the TCP endpoint and give your bus a name eg. "PLC1".
#drvAsynIPPortConfigure(portName, hostInfo, priority, noAutoConnect, noProcessEos)
#where the arguments are:
#	portName - The portName that is registered with asynManager.
#	hostInfo - The Internet host name, port number and optional IP protocol of the device (e.g.
#				"164.54.9.90:4002", "serials8n3:4002", "serials8n3:4002 TCP" or "164.54.17.43:5186 udp"). If no
#				protocol is specified, TCP will be used. Possible protocols are:
#		TCP,
#		UDP,
#		UDP* — UDP broadcasts. The address portion of the argument must be the network broadcast address
#				(e.g. "192.168.1.255:1234 UDP*").
#		HTTP — Like TCP but for servers which close the connection after each transaction.
#		COM — For Ethernet/Serial adapters which use the TELNET RFC 2217 protocol. This allows port parameters
#				(speed, parity, etc.) to be set with subsequent asynSetOption commands just as for local serial
#				ports. The default parameters are 9600-8-N-1 with no flow control.
#	priority - Priority at which the asyn I/O thread will run. If this is zero or missing, then
#	epicsThreadPriorityMedium is used.
#	noAutoConnect - Zero or missing indicates that portThread should automatically connect. Non-zero if explicit
#					connect command must be issued.
#	noProcessEos - If 0 then asynInterposeEosConfig is called specifying both processEosIn and processEosOut.
drvAsynIPPortConfigure($(RTUNAME),$(IPADDR):$(MODBUSDRVPORT),0,0,1)
#
#Configure the interpose for TCP
#modbusInterposeConfig(portName, linkType, timeoutMsec, writeDelayMsec)
#where the arguments are:
#	portName - Name of the asynIPPort or asynSerialPort previously created.
#	linkType - Modbus link layer type:
#		0 = TCP/IP
#		1 = RTU
#		2 = ASCII
#	timeoutMsec - The timeout in milliseconds for write and read operations to the underlying asynOctet driver.
#					This value is used in place of the timeout parameter specified in EPICS device support. If
#					zero is specified then a default timeout of 2000 milliseconds is used.
#	writeDelayMsec - The delay in milliseconds before each write from EPICS to the device. This is typically only
#						needed for Serial RTU devices. The Modicon Modbus Protocol Reference Guide says this must
#						be at least 3.5 character times, e.g. about 3.5ms at 9600 baud, for Serial RTU. The
#						default is 0.
modbusInterposeConfig($(RTUNAME), 0, $(RECVTIMEOUT), 0)
#
#Creating Modbus ports
#drvModbusAsynConfigure(portName, tcpPortName, slaveAddress, modbusFunction,
#                       modbusStartAddress, modbusLength, dataType,
#                       pollMsec, plcType);
#where the arguments are:
#	portName - Name of the modbus port to be created.
#	tcpPortName - Name of the asyn IP or serial port previously created.
#	slaveAddress - The address of the Modbus slave. This must match the configuration of the Modbus slave (PLC) for
#					RTU and ASCII. For TCP the slave address is used for the "unit identifier", the last field in
#					the MBAP header. The "unit identifier" is ignored by most PLCs, but may be required by some.
#	modbusFunction - Modbus function code (1, 2, 3, 4, 5, 6, 15, 16, 123 (for 23 read-only), or 223 (for 23
#					write-only)). 
#	modbusStartAddress - Start address for the Modbus data segment to be accessed.
#						(0-65535 decimal, 0-0177777 octal).
#						For absolute addressing this must be set to -1.
#	modbusLength - The length of the Modbus data segment to be accessed.
#					This is specified in bits for Modbus functions 1, 2, 5 and 15.
#					It is specified in 16-bit words for Modbus functions 3, 4, 6, 16, or 23.
#					Length limit is 2000 for functions 1 and 2, 1968 for functions 5 and 15,
#					125 for functions 3 and 4, and 123 for functions 6, 16, and 23.
#	dataType - This sets the default data type for this port. This is the data type used if the drvUser field of a
#				record is empty, or if it is MODBUS_DATA. The supported Modbus data types and correponding drvUser
#				fields are described in the table below.
#	pollMsec - Polling delay time in msec for the polling thread for read functions.
#				For write functions, a non-zero value means that the Modbus data should be read once when the
#				port driver is first created.
#	plcType - Type of PLC (e.g. Koyo, Modicon, etc.). 
#				This parameter is currently used to print information in asynReport. It is also used to treat Wago
#				devices specially if the plcType string contains the substring "Wago". See the note below.
#
# Read port
drvModbusAsynConfigure("$(RTUNAME)readOffset0",	$(RTUNAME), 0, 3, 1, 13, 0, 1000, "ABB")
drvModbusAsynConfigure("$(RTUNAME)readOffset200",	$(RTUNAME), 0, 3, 201, 12, 0, 1000, "ABB")
drvModbusAsynConfigure("$(RTUNAME)readOffset5000",	$(RTUNAME), 0, 3, 5000, 27, 7, 1000, "ABB")
# Write port
#drvModbusAsynConfigure("$(RTUNAME)write",	$(RTUNAME), 0, 6, -1, 2, 0, 0, "ABB")
####################
#
#



# Load the database defining your EPICS records
dbLoadRecords(rtu_pwr_j1_2ka1_2_ctrl_rtu_01.db, "RTUNAME = $(RTUNAME), SUBSTATIONNAME = $(SUBSTATIONNAME)")
