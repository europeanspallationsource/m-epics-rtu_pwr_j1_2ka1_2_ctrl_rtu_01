# Building H05 Power Distribution System ABB RTU

EPICS module to provide communications to/from ABB RTU over Modbus

* This is a module specific to a particular RTU (Building H05 Power Distribution System).
* This module contains the database with records that EPICS will read/write to this RTU.
* This module requires a **modbus** driver. This is done using **Makefile**.

If you need to create a new module for EPICS to communicate to an ABB RTU:

1. Create a copy of this module
2. Name accordingly
3. Modify database

## Startup Examples

`iocsh -r rtu_pwr_j1_2ka1_2_ctrl_rtu_01,1.0.0 -c 'requireSnippet(startup.cmd, "RTUNAME=PWR-J1KA1:Ctrl-RTU-01,IPADDR=172.17.0.9,MODBUSDRVPORT=502,RECVTIMEOUT=1000")'`

If ran as proper ioc service:
```
epicsEnvSet(RTUNAME, "PWR-J1KA1:Ctrl-RTU-01")
epicsEnvSet(IPADDR, "172.17.0.9")
epicsEnvSet(MODBUSDRVPORT, "502")
epicsEnvSet(RECVTIMEOUT, "1000")
require rtu_pwr_j1_2ka1_2_ctrl_rtu_01, 1.0.0
< ${REQUIRE_rtu_pwr_j1_2ka1_2_ctrl_rtu_01_PATH}/startup/startup.cmd
```
